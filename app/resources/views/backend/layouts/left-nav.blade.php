
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        @if (Auth::user()->image !== NULL)
        <div class="pull-left image">
          <img src="{{ Storage::disk('local')->url(Auth::user()->image) }}" class="img-circle" alt="User Image">
        </div>
        @else
        <div class="pull-left image">
          <img src="{{ asset('backend/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        @endif
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>


        @if (Gate::check('news.update') || Gate::check('news.delete'))
          <li class="treeview">
          <a href="{{ route('news.index')}}">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>News</span>          
          </a>
        </li>
        @endif

        <li class="header">ADMIN AREA</li>

        <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Admin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @if (Gate::check('user.create') || Gate::check('user.update') || Gate::check('user.delete'))
              <li><a href="{{ route('user.index') }}"><i class="fa fa-circle-o"></i> Users</a></li>
            @endif

             @if (Gate::check('role.create') || Gate::check('role.update') || Gate::check('role.delete'))
            <li><a href="{{ route('role.index') }}"><i class="fa fa-circle-o"></i> Roles</a></li>
            @endif

            @if (Gate::check('permission.create') || Gate::check('permission.update') || Gate::check('permission.delete'))
            <li><a href="{{ route('permission.index') }}"><i class="fa fa-circle-o"></i> Permissions</a></li>
            @endif

          </ul>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
