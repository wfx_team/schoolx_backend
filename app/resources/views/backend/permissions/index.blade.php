@extends('backend/layouts/app')

@section('headSection')
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Permissions
                <small>add, edit, delete permissions section</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Permissions</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Permission List</h3>

              @include('include.messages')

              @can('permission.create', Auth::user())
              <a href="{{ route('permission.create')}}" class="pull-right btn btn-primary">Add Permission</a>
              @endcan
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Permission Name</th>
                  <th>Permission For</th>
                  <th>Created at</th>
                  @can('permission.update', Auth::user())
                  <th>Edit</th>
                  @endcan

                  {{--  @can('permission.delete', Auth::user())
                  <th>Delete</th>
                  @endcan  --}}
                </tr>
                </thead>
                <tbody>
                @foreach($permissions as $permission)
                <tr>
                  <td>{{ $loop->index + 1 }}</td>
                  <td>{{ $permission->name}}</td>  
                  <td>{{ $permission->for_permission}}</td>  
                  
                  <td>{{ $permission->created_at}}</td>
                  <td><a href="{{ route('permission.edit', $permission->id)}}"><span class="glyphicon glyphicon-edit"></span></a></td>
                  {{--  <td>
                    <form id="delete-form-{{ $permission->id }}" method="post" action="{{ route('permission.destroy', $permission->id)}}" style="display:none;">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                        if(confirm('Are you sure, you want to delete this ?')) {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $permission->id}}').submit();
                        }
                        else
                        {
                            event.preventDefault();
                        }">
                        
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                  </td>  --}}
                </tr>      

                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Permission Name</th>
                  <th>Permission For</th>
                  <th>Created at</th>
                  @can('permission.update', Auth::user())
                  <th>Edit</th>
                  @endcan

                  {{--  @can('permission.delete', Auth::user())
                  <th>Delete</th>
                  @endcan  --}}
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
                </div><!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('footerSection')
    <script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
            });
        });
    </script>
@endsection