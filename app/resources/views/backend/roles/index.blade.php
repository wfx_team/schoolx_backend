@extends('backend/layouts/app')

@section('headSection')
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Roles
                <small>add, edit, delete roles section</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Roles</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Roles List</h3>
              @can('role.create', Auth::user())
              <a href="{{ route('role.create')}}" class="pull-right btn btn-primary">Add Role</a>
              @endcan
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Role Name</th>
                  {{--  <th>Publish</th>  --}}
                  <th>Created at</th>
                  @can('role.update', Auth::user())
                  <th>Edit</th>
                  @endcan

                  {{--  @can('role.delete', Auth::user())
                  <th>Delete</th>
                  @endcan  --}}
                </tr>
                </thead>
                <tbody>
                @foreach($roles as $role)
                <tr>
                  <td>{{ $loop->index + 1 }}</td>
                  <td>{{ $role->name}}</td>  
                  {{--  <td>
                    @if ($role->status == 1)
                        {{ "PUBLISH" }}
                    @else
                        {{ "NOT PUBLISH" }}
                    @endif
                </td>    --}}
                  <td>{{ $role->created_at}}</td>
                   @can('role.update', Auth::user())  
                  <td><a href="{{ route('role.edit', $role->id)}}"><span class="glyphicon glyphicon-edit"></span></a></td>
                  @endcan
                  {{--  @can('role.delete', Auth::user())
                  
                  <td>
                    <form id="delete-form-{{ $role->id }}" method="post" action="{{ route('role.destroy', $role->id)}}" style="display:none;">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                        if(confirm('Are you sure, you want to delete this ?')) {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $role->id}}').submit();
                        }
                        else
                        {
                            event.preventDefault();
                        }">
                        
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                  </td>  
                  @endcan
                  --}}
                </tr>      

                @endforeach
                </tbody>
                <tfoot>
               <tr>
                  <th>ID</th>
                  <th>Role Name</th>
                  {{--  <th>Publish</th>  --}}
                  <th>Created at</th>
                  @can('role.update', Auth::user())
                  <th>Edit</th>
                  @endcan

                  {{--  @can('role.delete', Auth::user())
                  <th>Delete</th>
                  @endcan  --}}
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
                </div><!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('footerSection')
    <script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
            });
        });
    </script>
@endsection