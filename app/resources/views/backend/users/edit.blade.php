@extends('backend/layouts/app')

@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
                <small>add, edit, delete users section</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Users</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Users Section</h3>
                        </div>

                        @include('include/messages')
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{ route('user.update', $user->id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PATCH')}}
                            <div class="box-body">
                                <!-- left column -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="news-head">Name <code>*</code></label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ $user->name}}" placeholder="Enter user name ..">
                                    </div>

                                    <div class="form-group">
                                        <label for="news-head">Email <code>*</code></label>
                                        <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" placeholder="Enter email address ..">
                                    </div>
                                    @can('user.delete', Auth::user())
                                    <div class="form-group">
                                        <label>Assign Role</label>
                                        <div class="row">
                                            @foreach ($roles as $role)
                                            <div class="col-lg-3">                                
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="role_id[]" value="{{ $role->id }}" 
                                                        
                                                        @foreach ($user->roles as $user_role)
                                                            @if ($user_role->id == $role->id)
                                                                checked
                                                            @endif
                                                        @endforeach
                                                        
                                                        > {{ $role->name }}
                                                    </label>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        
                                    </div>
                                    @endcan

                                    

                                </div>

                                <!-- right column -->
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="image">User Image</label>
                                            <br>
                                            <img src="{{ Storage::disk('local')->url($user->image) }}" style="width: 50px; height:20px">
                                            <br>
                                            <br>
                                        <input type="file" id="image" name="image">
                                        <p class="help-block">put user image here</p>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="news-head">Telephone </label>
                                        <input type="text" class="form-control" id="phone" name="phone" value="{{ $user->phone }}" placeholder="Enter phone number ..">
                                    </div>

                                    <div class="form-group">
                                        <label for="news-head">Address </label>
                                        <textarea class="form-control" name="address" placeholder="Enter address..." rows="3">{{ $user->address }}</textarea>
                                    </div>

                                    @can('user.delete', Auth::user())
                                    <br>
                                    <div class="form-group">
                                        <label for="news-head">Active User </label>
                                        <div class="checkbox" >
                                            <label>
                                                <input type="checkbox" name="status" value="1" @if ($user->status == 1) checked @endif> Active User
                                            </label>
                                        </div>
                                    </div>
                                    @endcan

                                    
                                    


                                    

                                </div>
                            </div>
                            <!-- /.box-body -->

                            {{--  <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">News description <code>*</code>
                                        <small>put news description here</small>
                                    </h3>              
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body pad">                                   
                                    <textarea class="textarea" id="editor1" name="description" placeholder="Place news contents here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                </div>  --}}

                                <input type="hidden" name="admin_id" value="{{ $user->admin_id }}">

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('user.index') }}" class="btn btn-warning">Back</a>
                                </div>
                            </div>             
                        </form>
                    </div>
                    <!-- /.box --> 
                </div><!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

