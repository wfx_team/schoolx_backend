<?php

namespace App\Policies;

use App\Model\admin\admin;
use App\Model\user\news;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the news.
     *
     * @param  \App\Model\user\User  $user
     * @param  \App\News  $news
     * @return mixed
     */
    public function view(admin $user)
    {
        
    }

    /**
     * Determine whether the user can create news.
     *
     * @param  \App\Model\user\User  $user
     * @return mixed
     */
    public function create(admin $user)
    {
        return $this->getPermission($user, 1);
    }

    /**
     * Determine whether the user can update the news.
     *
     * @param  \App\Model\user\User  $user
     * @param  \App\News  $news
     * @return mixed
     */
    public function update(admin $user)
    {
        return $this->getPermission($user, 2);
    }

    /**
     * Determine whether the user can delete the news.
     *
     * @param  \App\Model\user\User  $user
     * @param  \App\News  $news
     * @return mixed
     */
    public function delete(admin $user)
    {
        return $this->getPermission($user, 3);
    }

    protected function getPermission($user, $p_id)
    {
        foreach ($user->roles as $role) {
            foreach ($role->permissions as $permission) {
                if ($permission->id == $p_id) {
                    return true;
                }
            }
        }
        return false;
    }
}
