<?php

namespace App\Http\Controllers\Admin;

use App\Model\admin\permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;


class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = permission::all();
        return view ('backend.permissions.index', compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->can('permission.create')) {
            return view ('backend.permissions.create');
        } 
        
        return redirect(route('admin-slf'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50|unique:permissions',
            'for_permission' => 'required'
        ]);

        $permission = new permission;

        $permission->name = $request->name;
        $permission->for_permission = $request->for_permission;
        $permission->admin_id = $request->admin_id;

        $permission->save();

        return redirect(route('permission.index'))->with('message', 'Permission Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\admin\permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\admin\permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(permission $permission)
    {
        if (Auth::user()->can('permission.update')) {
            $permission = permission::where('id', $permission->id)->first();
            return view('backend.permissions.edit', compact('permission'));
        } 

        return redirect(route('admin-slf'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\admin\permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, permission $permission)
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'for_permission' => 'required'
        ]);

        $permission = permission::find($permission->id);

        $permission->name = $request->name;
        $permission->for_permission = $request->for_permission;
        $permission->admin_id = $request->admin_id;

        $permission->save();

        return redirect(route('permission.index'))->with('message', 'Permission Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\admin\permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(permission $permission)
    {
        permission::where('id', $permission->id)->delete();
        return redirect()->back()->with('message', 'Permission Deleted Successfully');
    }
}
