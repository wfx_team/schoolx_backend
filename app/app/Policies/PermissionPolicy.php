<?php

namespace App\Policies;

use App\Model\admin\admin;
use App\Model\user\permission;

use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the permission.
     *
     * @param  \App\Model\user\User  $user
     * @param  \App\permission  $permission
     * @return mixed
     */
    public function view(admin $user)
    {
        //
    }

    /**
     * Determine whether the user can create permissions.
     *
     * @param  \App\Model\user\User  $user
     * @return mixed
     */
    public function create(admin $user)
    {
        return $this->getPermission($user, 39);
    }

    /**
     * Determine whether the user can update the permission.
     *
     * @param  \App\Model\user\User  $user
     * @param  \App\permission  $permission
     * @return mixed
     */
    public function update(admin $user)
    {
        return $this->getPermission($user, 40);
    }

    /**
     * Determine whether the user can delete the permission.
     *
     * @param  \App\Model\user\User  $user
     * @param  \App\permission  $permission
     * @return mixed
     */
    public function delete(admin $user)
    {
        return $this->getPermission($user, 41);
    }

    protected function getPermission($user, $p_id)
    {
        foreach ($user->roles as $role) {
            foreach ($role->permissions as $permission) {
                if ($permission->id == $p_id) {
                    return true;
                }
            }
        }
        return false;
    }
}
