<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use App\Model\admin\admin;
use App\Model\admin\role;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = admin::all();
        return view('backend/users/index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->can('user.create')) {
            $roles = role::all();
            return view('backend/users/create', compact('roles'));
        } 
        
        return redirect(route('admin-slf'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:admins|email|max:255',
            'password' => 'required|confirmed',
        ]);

         $admin = new admin;

        if ($request->hasFile('image')) {
            $imageName = $request->image->store('public');
            $admin->image = $imageName;
        }

        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->password = bcrypt($request->password);
        $admin->status = $request->status? : $request->status=0;
        //$admin->password_confirmation = $request->password_confirmation;
        $admin->phone = $request->phone;
        $admin->address = $request->address; 
        $admin->admin_id = $request->admin_id; 

        $admin->save();

        $admin->roles()->sync($request->role_id);

        return redirect(route('user.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->can('user.update')) {
            $user = admin::where('id', $id)->first();
            $roles = role::all();
            return view('backend.users.edit', compact('user', 'roles'));
        } 

        return redirect(route('admin-schoolx'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|max:255',
            //'password' => 'required|confirmed',
            //'role_id' => 'required',
        ]);

        if ($request->hasFile('image')) {
            $imageName = $request->image->store('public');
        } else {
            $result = admin::where('id', $id)->first();
            $imageName = $result->image;
        }

        $admin = admin::find($id);

        //return $admin;

        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->image = $imageName;
        //$admin->password = $request->password;
        $admin->status = $request->status? : $request->status=0;
        //$admin->password_confirmation = $request->password_confirmation;
        $admin->phone = $request->phone;
        $admin->address = $request->address;
        $admin->admin_id = $request->admin_id;
        //$admin->role_id = $request->role_id;

        $admin->save();

        $admin->roles()->sync($request->role_id);

        return redirect(route('user.index'));
    } 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        admin::where('id', $id)->delete();
        return redirect()->back();
    }
}
