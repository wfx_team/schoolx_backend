@extends('backend/layouts/app')

@section('headSection')
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
                <small>add, edit, delete users section</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Users</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
            <div class="box-header">
              <h3 class="box-title">User List</h3>
              @can('user.create', Auth::user())
              <a href="{{ route('user.create')}}" class="pull-right btn btn-primary">Add User</a>
              @endcan
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>User ID</th>
                  <th>User Name</th>
                  <th>Assigned Role</th>
                  <th>Status</th>
                  <th>Email</th>
                  <th>Created at</th>
                  @can('user.update', Auth::user())
                    <th>Edit</th>
                  @endcan

                  @can('user.delete', Auth::user())
                    <th>Delete</th>
                  @endcan
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                <tr>
                  <td>{{ $loop->index + 1 }}</td>
                  <td>{{ $user->name}}</td>  
                  <td>
                    @foreach ($user->roles as $role)
                        {{ $role->name}},
                    @endforeach
                  </td>
                  <td>{{ $user->status? 'Active' : 'Not Active'}}</td>  
                  <td>{{ $user->email}}</td>  
                  {{--  <td><img src="{{ Storage::disk('local')->url($user->image) }}" style="width: 50px; height:20px"></td>    --}}
                  {{--  <td>
                    @if ($user->status == 1)
                        {{ "PUBLISH" }}
                    @else
                        {{ "NOT PUBLISH" }}
                    @endif
                </td>      --}}
                  <td>{{ $user->created_at}}</td> 
                  @can('user.update', Auth::user()) 
                  <td><a href="{{ route('user.edit', $user->id)}}"><span class="glyphicon glyphicon-edit"></span></a></td>
                  @endcan

                  @can('user.delete', Auth::user())
                  <td>
                    <form id="delete-form-{{ $user->id }}" method="post" action="{{ route('user.destroy', $user->id)}}" style="display:none;">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                        if(confirm('Are you sure, you want to delete this ?')) {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $user->id}}').submit();
                        }
                        else
                        {
                            event.preventDefault();
                        }">
                        
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                  </td>
                  @endcan
                </tr>      

                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>User ID</th>
                  <th>User Name</th>
                  <th>Assigned Role</th>
                  <th>Status</th>
                  <th>Email</th>
                  <th>Created at</th>
                  @can('user.update', Auth::user())
                    <th>Edit</th>
                  @endcan
                  
                  @can('user.delete', Auth::user())
                    <th>Delete</th>
                  @endcan
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
                </div><!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('footerSection')
    <script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
            });
        });
    </script>
@endsection