@extends('backend/layouts/app')

@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Roles
                <small>add, edit, delete roles section</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Roles</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Roles Section</h3>
                        </div>

                        @include('include/messages')
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{ route('role.store') }}" method="post">
                        {{ csrf_field() }}
                            <div class="box-body">
                                <!-- left column -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="news-head">Role Name <code>*</code></label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Role name ..">
                                    </div>
                    
                                    
                                </div>

                                <!-- right column -->
                                 <div class="col-md-6">
                                    <br>
                                    <div class="checkbox" style="visibility: hidden">
                                        <label>
                                            <input type="checkbox" name="status"> Publish Role
                                        </label>
                                    </div>
                                    <br>
                                </div> 
                               
                                <div class="col-lg-4">
                                    <label for="news-head">Posts Permissions</label>
                                    @foreach ($permissions as $permission)
                                        @if ($permission->for_permission == 'post')
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="permission[]" value="{{ $permission->id}}">{{ $permission->name }}</label>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                                <div class="col-lg-4">
                                    <label for="news-head">Users Permissions</label>
                                    @foreach ($permissions as $permission)
                                        @if ($permission->for_permission == 'user')
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="permission[]" value="{{ $permission->id}}">{{ $permission->name }}</label>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                                <div class="col-lg-4">
                                    <label for="news-head">Other Permissions</label>
                                    @foreach ($permissions as $permission)
                                        @if ($permission->for_permission == 'other')
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="permission[]" value="{{ $permission->id}}">{{ $permission->name }}</label>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>




                            </div>

                            
                            <!-- /.box-body -->

                            <div class="box">

                                <input type="hidden" name="admin_id" value="1">

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('role.index') }}" class="btn btn-warning">Back</a>
                                </div>
                            </div>             
                        </form>
                    </div>
                    <!-- /.box --> 
                </div><!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection