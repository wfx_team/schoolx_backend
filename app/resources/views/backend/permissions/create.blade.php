@extends('backend/layouts/app')

@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Permissions
                <small>add, edit, delete permissions section</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Permissions</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Permission Section</h3>
                        </div>

                        @include('include/messages')
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{ route('permission.store') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                            <div class="box-body">
                                <!-- left column -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="news-head"> Permission Name<code>*</code></label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Permission name ..">
                                    </div>

                                    <div class="form-group">
                                        <label for="for-permission"> Permission for</label>
                                        <select name="for_permission" id="for_permission" class="form-control">
                                            <option selected disable>Select Permission for </option>
                                            <option value="user">User</option>
                                            <option value="post">Post</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                </div>

                                <!-- right column -->
                                <div class="col-md-6">

                                    
                                    
                                </div>
                            </div>
                            <!-- /.box-body -->

                            {{--  <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">News description <code>*</code>
                                        <small>put news description here</small>
                                    </h3>              
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body pad">                                   
                                    <textarea class="textarea" id="editor1" name="description" placeholder="Place news contents here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                </div>  --}}

                                <input type="hidden" name="admin_id" value="1">

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('permission.index') }}" class="btn btn-warning">Back</a>
                                </div>
                            </div>             
                        </form>
                    </div>
                    <!-- /.box --> 
                </div><!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

