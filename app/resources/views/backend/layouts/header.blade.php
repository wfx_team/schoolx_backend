
    <!-- Logo -->
    <a href="/admin-schoolx" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>ABC</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Web</b>ABC</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            @if (Auth::user()->image !== NULL)
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ Storage::disk('local')->url(Auth::user()->image) }}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            @else
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ asset('backend/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            @endif
            <ul class="dropdown-menu">
              <!-- User image -->
              @if (Auth::user()->image !== NULL)
              <li class="user-header">
                <img src="{{ Storage::disk('local')->url(Auth::user()->image) }}"  alt="user image" class="img-circle">
                
                <p>
                  {{ Auth::user()->name }} - SchoolX User
                  <small>Member since {{ Auth::user()->created_at->toFormattedDateString() }}</small> 
                </p>
              </li>
              @else 
              <li class="user-header">
                <img src="{{ asset('backend/dist/img/user2-160x160.jpg')}}"  alt="user image" class="img-circle">
                
                <p>
                  {{ Auth::user()->name }} - SchoolX User
                  <small>Member since {{ Auth::user()->created_at->toFormattedDateString() }}</small> 
                </p>
              </li>
              @endif           
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-warning btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();" class="btn btn-primary btn-flat">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
