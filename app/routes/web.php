<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Site Route
//Route::get('/', 'HomeController@index');
//Route::get('/open', 'HomeController@index');
Route::get('/', 'HomeController@index1');
Route::get('/home', 'HomeController@index1')->name('home');

// All Admin Route
Route::group(['namespace' => 'Admin'], function() {

    // Admin Auth Routes
    Route::get('admin-login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('admin-login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('register-schoolx', 'Auth\RegisterController@showRegistrationForm')->name('admin.register');
    Route::post('register-schoolx', 'Auth\RegisterController@register');
    
    // Dashboard Routes
    Route::get('admin-schoolx', 'AdminController@index')->name('admin-slf');

    // Users Route
    Route::resource('admin-schoolx/user', 'UserController');

    // Roles Route
    Route::resource('admin-schoolx/role', 'RoleController');

    // Permission Route
    Route::resource('admin-schoolx/permission', 'PermissionController');

    // News Routes
    Route::resource('admin-schoolx/news', 'NewsController');
    

});

Auth::routes();







