<?php

namespace App\Mail;

use App\Model\user\course_registration;
use App\Model\user\course_name;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $course_reg;
    public $courses;

    public function __construct(course_registration $course_reg, course_name $courses)
    {
        $this->course_reg = $course_reg;
        $this->courses = $courses;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('frontend.emails.email');
    }
}
