<?php

namespace App\Policies;

use App\Model\admin\admin;
use App\Model\user\role;

use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the role.
     *
     * @param  \App\Model\user\User  $user
     * @param  \App\role  $role
     * @return mixed
     */
    public function view(admin $user)
    {
        //
    }

    /**
     * Determine whether the user can create roles.
     *
     * @param  \App\Model\user\User  $user
     * @return mixed
     */
    public function create(admin $user)
    {
        return $this->getPermission($user, 36);
    }

    /**
     * Determine whether the user can update the role.
     *
     * @param  \App\Model\user\User  $user
     * @param  \App\role  $role
     * @return mixed
     */
    public function update(admin $user)
    {
        return $this->getPermission($user, 37);
    }

    /**
     * Determine whether the user can delete the role.
     *
     * @param  \App\Model\user\User  $user
     * @param  \App\role  $role
     * @return mixed
     */
    public function delete(admin $user)
    {
        return $this->getPermission($user, 38);
    }

    protected function getPermission($user, $p_id)
    {
        foreach ($user->roles as $role) {
            foreach ($role->permissions as $permission) {
                if ($permission->id == $p_id) {
                    return true;
                }
            }
        }
        return false;
    }
}
