<?php

namespace App\Model\admin;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class admin extends Authenticatable
{
   use Notifiable;

   protected $fillable = ['name', 'email', 'password'];

   public function roles()
   {
       return $this->belongsToMany('App\Model\admin\role');
   }

   public function getNameAttribute($value)
   {
        return ucfirst($value);
   }

  
}

