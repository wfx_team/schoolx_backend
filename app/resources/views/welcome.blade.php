<!DOCTYPE html>

<html>
<head>
    @include('frontend/layouts/head')
</head>

<body>
    <!-- header section -->
    <div class="header" >
        @include('frontend/layouts/header')
    </div>
    <!-- End header section -->

    <!-- Navigation section -->
    <nav class="navbar navbar-default navbar-static-top">
        @include('frontend/layouts/navigation')
    </nav>
    <!-- End Navigation section -->

    <!-- carousel section -->
    <div class="container">
        @include('frontend/layouts/carousel')
    </div>
    <!-- End carousel section -->

    <!-- content --> 
    <div class="content">
        <div class="container">
            <div class="row">
                <!-- start left bar -->				
                <div class="col-sm-9">
                    <!-- start News area -->
                    <div class="row">
                        <fieldset class="form-group">
                            <legend>News</legend>
                        </fieldset>
                        <!-- first row - news -->
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail">
                                    <img src="{{ asset('frontend/images/education-a.jpg') }}" alt="..."> 
                                    <div class="caption" style="padding: 6px;padding-bottom: 32px;">
                                        <h5 style="margin-top: 2px;"><b>Education education education Education </b></h5>
                                        <p style="font-size: 12px;">dummy content dummy content dummy content dummy content dummy content dummy content...</p>
                                        <button type="button" class="btn btn-primary" style="font-size: 10px;float: right;" data-toggle="modal" data-target="#newsModal">Read more</button>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail">
                                    <img src="{{ asset('frontend/images/education-a.jpg') }}" alt="..."> 
                                    <div class="caption" style="padding: 6px;padding-bottom: 32px;">
                                        <h5 style="margin-top: 2px;"><b>Education education education Education </b></h5>
                                        <p style="font-size: 12px;">dummy content dummy content dummy content dummy content dummy content dummy content...</p>
                                        <button type="button" class="btn btn-primary" style="font-size: 10px;float: right;" data-toggle="modal" data-target="#newsModal">Read more</button>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail">
                                    <img src="{{ asset('frontend/images/education-a.jpg') }}" alt="..."> 
                                    <div class="caption" style="padding: 6px;padding-bottom: 32px;">
                                        <h5 style="margin-top: 2px;"><b>Education education education Education </b></h5>
                                        <p style="font-size: 12px;">dummy content dummy content dummy content dummy content dummy content dummy content...</p>
                                        <button type="button" class="btn btn-primary" style="font-size: 10px;float: right;" data-toggle="modal" data-target="#newsModal">Read more</button>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end first row - news -->                   
                        <!-- second row -->
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail">
                                    <img src="{{ asset('frontend/images/education-a.jpg') }}" alt="..."> 
                                    <div class="caption" style="padding: 6px;padding-bottom: 32px;">
                                        <h5 style="margin-top: 2px;"><b>Education education education Education </b></h5>
                                        <p style="font-size: 12px;">dummy content dummy content dummy content dummy content dummy content dummy content...</p>
                                        <button type="button" class="btn btn-primary" style="font-size: 10px;float: right;" data-toggle="modal" data-target="#newsModal">Read more</button>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail">
                                    <img src="{{ asset('frontend/images/education-a.jpg') }}" alt="..."> 
                                    <div class="caption" style="padding: 6px;padding-bottom: 32px;">
                                        <h5 style="margin-top: 2px;"><b>Education education education Education </b></h5>
                                        <p style="font-size: 12px;">dummy content dummy content dummy content dummy content dummy content dummy content...</p>
                                        <button type="button" class="btn btn-primary" style="font-size: 10px;float: right;" data-toggle="modal" data-target="#newsModal">Read more</button>
                                        
                                    </div>
                                </div>
                            </div>                            
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail">
                                    <img src="{{ asset('frontend/images/education-a.jpg') }}" alt="..."> 
                                    <div class="caption" style="padding: 6px;padding-bottom: 32px;">
                                        <h5 style="margin-top: 2px;"><b>Education education education Education </b></h5>
                                        <p style="font-size: 12px;">dummy content dummy content dummy content dummy content dummy content dummy content...</p>
                                        <button type="button" class="btn btn-primary" style="font-size: 10px;float: right;" data-toggle="modal" data-target="#newsModal">Read more</button>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-12">
                            <nav aria-label="Page navigation" class="pagination_course">
                            <ul class="pagination">
                                <li>
                                <a href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                                </li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                                </li>
                            </ul>
                            </nav>
                            </div>
                            
                        </div>
                        <!-- end second row - news -->
                    </div>
                    <!-- end news area -->

                    <!-- start Available courses area -->
                    <div class="row">                       
                        <fieldset class="form-group">
                            <legend>Available Courses</legend>
                        </fieldset>
                        <!-- search criteria -->
                        <div class="row">                           
                            <div class="col-sm-6 col-lg-5">
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Course Area</label>
                                    <select class="form-control">
                                        <option>-- Select Course Area --</option>
                                        <option value="IT">Information Technology</option>
                                        <option value="psycology">Psycology</option>
                                    </select>
                                </div>
                            </div><!-- /.col-lg-6 -->

                            <div class="col-sm-6 col-lg-5">
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Course Type</label>
                                    <select class="form-control">
                                        <option>-- Select Course Type --</option>
                                        <option value="degree">Degree</option>
                                        <option value="diploma">Diploma</option>
                                        <option value="certificate">Certificate</option>                
                                    </select>
                                </div>
                            </div><!-- /.col-lg-6 -->

                            <div class="col-sm-6 col-lg-2">
                                <label for="formGroupExampleInput2" style="visibility: hidden;">Search Course</label>
                                <span class="">
                                    <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                </span>               
                            </div><!-- /.col-lg-6 -->
                        </div>
                        <!-- end search criteria -->

                        <!-- course criteria-->
                        <div class="row">                     
                            <div class="col-sm-6 col-md-12">
                                <div class="course-thumbnail" style="border: 1px solid #2e6da4;border-color: #2e6da4;border-radius: 4px;padding: 4px;margin-bottom: 20px;text-align: justify;min-height: 105px;">
                                    <div class="col-xs-6 col-md-3">
                                        <a href="#" class="thumbnail" data-toggle="modal" data-target="#courseModal">
                                            <img src="{{ asset('frontend/images/course_1.jpg') }}" alt="...">
                                        </a>
                                    </div>
                                    <div class=""> 
                                        <h4><a href="#" data-toggle="modal" data-target="#courseModal">Course Name 1</a></h4>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy ....</p>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-12">
                                <div class="course-thumbnail" style="border: 1px solid #2e6da4;border-color: #2e6da4;border-radius: 4px;padding: 4px;margin-bottom: 20px;text-align: justify;min-height: 105px;">
                                    <div class="col-xs-6 col-md-3">
                                        <a href="#" class="thumbnail" data-toggle="modal" data-target="#courseModal">
                                            <img src="{{ asset('frontend/images/course_1.jpg') }}" alt="...">
                                        </a>
                                    </div>
                                    <div class=""> 
                                        <h4><a href="#" data-toggle="modal" data-target="#courseModal">Course Name 2</a></h4>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy ....</p>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-12">
                                <div class="course-thumbnail" style="border: 1px solid #2e6da4;border-color: #2e6da4;border-radius: 4px;padding: 4px;margin-bottom: 20px;text-align: justify;min-height: 105px;">
                                    <div class="col-xs-6 col-md-3">
                                        <a href="#" class="thumbnail" data-toggle="modal" data-target="#courseModal">
                                            <img src="{{ asset('frontend/images/course_1.jpg') }}" alt="...">
                                        </a>
                                    </div>
                                    <div class=""> 
                                        <h4><a href="#" data-toggle="modal" data-target="#courseModal">Course Name 3</a></h4>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy ....</p>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end course criteria-->                      
                    </div>
                    <!-- end Available courses area -->


                    <!-- Available event area -->
                    <div class="row">
                        <fieldset class="form-group">
                            <legend>Event Calender</legend>
                        </fieldset>
                        <!-- search event calender -->
                        <div class="row">   
                            <!-- <p style=""><span style="font-weight: bold">Search Event : </span><input type="text" class="form-control" id="datepicker"></p> -->
                            <div class="col-sm-6 col-lg-4">
                                <label for="formGroupExampleInput2">Search Event</label>
                                <div class="input-group form-group">
                                    <input type="text" class="form-control" placeholder="Check daily Event..." id="datepicker">
                                    <span class="input-group-btn" >
                                        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></button>
                                    </span>
                                </div>
                            </div>
                            <!-- /.col-lg-12 -->                       
                        </div>
                        <!-- end search event calender -->

                        <!-- event list -->
                        <div class="row">
                            <!-- first event -->
                            <div class="col-sm-6 col-md-12" data-toggle="modal" data-target="#eventModal">
                                <div class="course-thumbnail" style="border: 1px solid #2e6da4;border-color: #64B5F6;background-color: #f5f5f5;margin-bottom: 10px;text-align: justify;min-height: 80px;">
                                    <div class="col-xs-4 col-md-2" style="padding: 0px">
                                        <a class="" href="#">
                                            <img alt="..." src="{{ asset('frontend/images/img_sm_4.png') }}" style="width: 100px; height:80px">
                                        </a>
                                    </div>
                                    <div class="col-xs-4 col-md-2" style="padding: 0px">
                                        <h4 style="margin: 1px !important;"><a>Location</a></h4>
                                        <p>Lecture Hall 04</p>
                                    </div>
                                    <div class="col-xs-4 col-md-2" style="padding: 0px">
                                        <h4 style="margin: 1px !important;"><a>Date & Time</a></h4>
                                        <p>14 Aug 2017</p>
                                        <p>8.00am - 11.00am</p>
                                    </div>
                                    <div class="col-xs-4 col-md-4" style="padding: 0px">
                                        <h4 style="margin: 1px !important;"><a>Programe</a></h4>
                                        <p>Information Technology</p>
                                        <p>this course include ...</p>
                                        
                                    </div>
                                    <div class="col-xs-4 col-md-2" style="padding: 0px">
                                        <h4 style="margin: 1px !important;"><a>Institute</a></h4>
                                        <p>SriLanka Foundation</p>                                        
                                    </div>                                   
                                </div>
                            </div>
                            <!-- end first event --> 

                            <!-- second event -->
                            <div class="col-sm-6 col-md-12" data-toggle="modal" data-target="#eventModal">
                                <div class="course-thumbnail" style="border: 1px solid #2e6da4;border-color: #64B5F6;background-color: #f5f5f5;margin-bottom: 10px;text-align: justify;min-height: 80px;">
                                    <div class="col-xs-4 col-md-2" style="padding: 0px">
                                        <a class="" href="#">
                                            <img alt="..." src="{{ asset('frontend/images/img_sm_4.png') }}" style="width: 100px; height:80px">
                                        </a>
                                    </div>
                                    <div class="col-xs-4 col-md-2" style="padding: 0px">
                                        <h4 style="margin: 1px !important;"><a>Location</a></h4>
                                        <p>Lecture Hall 04</p>
                                    </div>
                                    <div class="col-xs-4 col-md-2" style="padding: 0px">
                                        <h4 style="margin: 1px !important;"><a>Date & Time</a></h4>
                                        <p>14 Aug 2017</p>
                                        <p>8.00am - 11.00am</p>
                                    </div>
                                    <div class="col-xs-4 col-md-4" style="padding: 0px">
                                        <h4 style="margin: 1px !important;"><a>Programe</a></h4>
                                        <p>Information Technology</p>
                                        <p>this course include ...</p>
                                        
                                    </div>
                                    <div class="col-xs-4 col-md-2" style="padding: 0px">
                                        <h4 style="margin: 1px !important;"><a>Institute</a></h4>
                                        <p>SriLanka Foundation</p>                                        
                                    </div>                                   
                                </div>                               
                            </div>
                            <!-- end second event --> 

                            <!-- third event -->
                            <div class="col-sm-6 col-md-12" data-toggle="modal" data-target="#eventModal">
                                <div class="course-thumbnail" style="border: 1px solid #2e6da4;border-color: #64B5F6;background-color: #f5f5f5;margin-bottom: 10px;text-align: justify;min-height: 80px;">
                                    <div class="col-xs-4 col-md-2" style="padding: 0px">
                                        <a class="" href="#">
                                            <img alt="..." src="{{ asset('frontend/images/img_sm_4.png') }}" style="width: 100px; height:80px">
                                        </a>
                                    </div>
                                    <div class="col-xs-4 col-md-2" style="padding: 0px">
                                        <h4 style="margin: 1px !important;"><a>Location</a></h4>
                                        <p>Lecture Hall 04</p>
                                    </div>
                                    <div class="col-xs-4 col-md-2" style="padding: 0px">
                                        <h4 style="margin: 1px !important;"><a>Date & Time</a></h4>
                                        <p>14 Aug 2017</p>
                                        <p>8.00am - 11.00am</p>
                                    </div>
                                    <div class="col-xs-4 col-md-4" style="padding: 0px">
                                        <h4 style="margin: 1px !important;"><a>Programe</a></h4>
                                        <p>Information Technology</p>
                                        <p>this course include ...</p>
                                        
                                    </div>
                                    <div class="col-xs-4 col-md-2" style="padding: 0px">
                                        <h4 style="margin: 1px !important;"><a>Institute</a></h4>
                                        <p>SriLanka Foundation</p>                                        
                                    </div>                                   
                                </div>                               
                            </div>
                            <!-- end third event -->

                        </div>
                        <!-- end event list -->
                    </div>
                    <!-- end Available event area -->                
                </div>
                <!-- end left bar -->

                <!-- start right bar -->
                <div class="col-sm-3" style="background-color: #f5f5f5">
                    <!-- start chairman message -->
                    <div class="row">                        
						<fieldset class="form-group">
                            <legend>Chairman Message</legend> 
                        </fieldset>
                        <div class="thumbnail" style="background-color: #f5f5f5;border: 0px;">
                            <img src="{{ asset('frontend/images/mr_sarrath kongahage.jpg') }}" alt="..."> 
                            <div class="caption" style="padding: 6px;padding-bottom: 32px;">
                                <h4 style="margin-top: 2px;"><b>Senior Attorney Mr. Sarrath  Kongahage </b></h4>
                                <p style="font-size: 12px;">dummy content dummy content dummy content dummy content dummy content dummy content...</p>
                                <button type="button" class="btn btn-primary" style="font-size: 10px;float: right;" data-toggle="modal" data-target="#messageModal">Read more</button>									
                            </div>
                        </div>
                    </div>
                    <!-- end chairman message-->

                    <!-- banner message-->
                    <div class="row">
                        <div class="col-xs-6 col-md-12">
                            <a href="#" class="thumbnail">
                            <img src="{{ asset('frontend/images/notice_1.jpg') }}" alt="notice 1">
                            </a>
                        </div>

                        <div class="col-xs-6 col-md-12">
                            <a href="#" class="thumbnail">
                            <img src="{{ asset('frontend/images/notice_2.jpg') }}" alt="notice 2">
                            </a>
                        </div>

                        <div class="col-xs-6 col-md-12">
                            <a href="#" class="thumbnail">
                            <img src="{{ asset('frontend/images/notice_3.jpg') }}" alt="notice 3">
                            </a>
                        </div>
                    </div>
                    <!-- end banner messages -->

                    <!-- registration links -->
                    <div class="row">
                        <div class="col-xs-6 col-md-12">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <!-- course registration link -->                           
                                    <div class="panel panel-default" style="margin-bottom: 15px; !important">
                                        <div class="panel-heading" role="tab" id="headingOne" style="color: #fff !important;background-color: #337ab7 !important;">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    Course Registration
                                                </a>
                                            </h4>
                                        </div>

                                        <div id="collapseOne_no_need" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                            <div class="panel-body">
                                            </div>
                                        </div>                               
                                    </div>                           
                                    <!-- end course registration link -->

                                    <!-- online booking link -->
                                    <div class="panel panel-default" style="margin-bottom: 15px; !important">
                                        <div class="panel-heading" role="tab" id="headingTwo" style="color: #fff !important;background-color: #337ab7 !important;">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    Online Booking
                                                </a>
                                            </h4>
                                        </div>

                                        <div id="collapseTwo_no need" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                            <div class="panel-body">                                   
                                            </div>
                                        </div>                               
                                    </div>
                                    <!-- end online booking link -->
                                                        
                            </div>
                        </div>
                    </div>
                    <!-- end registration links -->

                    <!-- image gallery -->
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-sm-6 col-md-12">
                            <fieldset class="form-group">
                                <legend>Image Gallery</legend> 
                            </fieldset>

                            <div class="carousel slide article-slide" id="article-photo-carousel">
                            </div>

                            <div class="carousel-inner cont-slider" data-toggle="modal" data-target="#imageModal">
                                <div class="item active">
                                    <img alt="" title="" src="{{ asset('frontend/images/img_1.jpg') }}">
                                </div>                       
                            </div>
                        </div>
                        <!-- end image gallery -->
                    </div>
 
                    <!-- video gallery -->
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-sm-6 col-md-12">
                            <fieldset class="form-group">
                                <legend>Video Gallery</legend> 
                            </fieldset>

                            <div class="carousel slide article-slide" id="article-photo-carousel">
                            </div>

                            <div class="carousel-inner cont-slider" data-toggle="modal" data-target="#videoModal">
                                <video width="300" height="200" controls>
                                    <source src="{{ asset('frontend/video/mov_bbb.mp4') }}" type="video/mp4">
                                    <source src="{{ asset('frontend/video/mov_bbb.ogg') }}" type="video/ogg">
                                    Your browser does not support the video tag.
                                </video>                      
                            </div>
                        </div>
                    </div>
                    <!-- end video gallery -->

                    <!-- comments box-->
                    <body onLoad="scrollDiv_init()" style="">
                        <fieldset class="form-group">
                            <legend>Comments</legend> 
                        </fieldset>
                        <div id="MyDivName" style="overflow:auto;width:100%;height:300px" onMouseOver="pauseDiv()" onMouseOut="resumeDiv()" data-toggle="modal" data-target="#commentModal">
                            <div class="col-sm-6 col-md-12">
                                <!-- first comment -->
                                <div class="course-thumbnail" style="border: 1px solid #2e6da4;border-color: #2e6da4;border-radius: 4px;padding: 4px;margin-bottom: 20px;text-align: justify;min-height: 105px;">
                                    <div class="col-xs-6 col-md-4">
                                        <a href="#" class="thumbnail">
                                            <img src="{{ asset('frontend/images/commentor_1.jpg') }}" alt="...">
                                        </a>
                                    </div>
                                    <div class=""> 
                                        <h4><a href="#" >Prof. Senarathna</a></h4>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy ....</p>
                                        
                                    </div>
                                </div>
                                <!-- end first comment -->
                                <!-- second comment -->
                                <div class="course-thumbnail" style="border: 1px solid #2e6da4;border-color: #2e6da4;border-radius: 4px;padding: 4px;margin-bottom: 20px;text-align: justify;min-height: 105px;">
                                    <div class="col-xs-6 col-md-4">
                                        <a href="#" class="thumbnail" >
                                            <img src="{{ asset('frontend/images/commentor_1.jpg') }}" alt="...">
                                        </a>
                                    </div>
                                    <div class=""> 
                                        <h4><a href="#" >Prof. Senarathna</a></h4>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy ....</p>
                                        
                                    </div>
                                </div>
                                <!-- end second comment -->
                                <!-- third comment -->
                                <div class="course-thumbnail" style="border: 1px solid #2e6da4;border-color: #2e6da4;border-radius: 4px;padding: 4px;margin-bottom: 20px;text-align: justify;min-height: 105px;">
                                    <div class="col-xs-6 col-md-4">
                                        <a href="#" class="thumbnail" >
                                            <img src="{{ asset('frontend/images/commentor_1.jpg') }}" alt="...">
                                        </a>
                                    </div>
                                    <div class=""> 
                                        <h4><a href="#" >Prof. Senarathna</a></h4>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy ....</p>
                                        
                                    </div>
                                </div>
                                <!-- end third comment -->                     
                            </div>
                        </div>
                    </body>
                    <!-- end comments box-->

                </div>
                <!-- end right bar -->

            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end content -->

    <!-- footer section -->
    <div class="main-footer" style="background-color: #7a9cbf !important;">
        @include('frontend/layouts/footer')
    </div>
    <!-- end footer section -->

    <!-- modal section -->

    <!-- Modal news section -->
    <div class="modal fade" id="newsModal" tabindex="-1" role="dialog" aria-labelledby="newsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow: auto;max-height:500px;">
                <div class="modal-header" style="background-color: #337ab7 !important;border-bottom: 0px !important;">
                    <h5 class="modal-title" id="newsModalLabel" style="font-weight: bold;color: #fff">Education education education Education
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <!--<span aria-hidden="true" style="color: red">&times;</span>-->
                    <span class="glyphicon glyphicon-remove" style="color:red !important" aria-hidden="true"></span>
                    </button>
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="{{ asset('frontend/images/education-a.jpg') }}" alt="..."> 
                                
                            </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <p>
                            <span style="font-weight: bold">Date : 2017 August 25</span>
                        </p>
                        <p>
                            <span style="font-weight: bold">Published By : W.A Silva</span>
                        </p>
                    </div>
                    </div>
                    <p style="text-align: justify;">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    </p>

                    <p style="text-align: justify;">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    </p>
                    <p style="text-align: justify;">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    </p>
                </div>
                <div class="modal-footer" style="background-color: #337ab7 !important;border-top: 0px !important">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end news modal popup -->

    <!-- Modal course section -->
    <div class="modal fade" id="courseModal" tabindex="-1" role="dialog" aria-labelledby="newsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow: auto;max-height:500px;">
                <div class="modal-header" style="background-color: #337ab7 !important;border-bottom: 0px !important;">
                    <h5 class="modal-title" id="newsModalLabel" style="font-weight: bold;color: #fff">Course Name 1
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <!--<span aria-hidden="true" style="color: red">&times;</span>-->
                        <span class="glyphicon glyphicon-remove" style="color:red !important" aria-hidden="true"></span>
                        </button>
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="{{ asset('frontend/images/course_1.jpg') }}" alt="...">                                     
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <p>
                                <span style="font-weight: bold">Date : 2017 August 25</span>
                            </p>
                            <p>
                                <span style="font-weight: bold">Published By : W.A Silva</span>
                            </p>
                        </div>
                    </div>
                    <p style="text-align: justify;">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    </p>

                    <p style="text-align: justify;">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    </p>
                    <p style="text-align: justify;">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    </p>
                </div>
                <div class="modal-footer" style="background-color: #337ab7 !important;border-top: 0px !important">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Register</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal popup -->

    <!-- Modal message section -->
    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="newsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow: auto;max-height:500px;">
            <div class="modal-header" style="background-color: #337ab7 !important;border-bottom: 0px !important;">
                <h5 class="modal-title" id="newsModalLabel" style="font-weight: bold;color: #fff">Chairman Message
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <!--<span aria-hidden="true" style="color: red">&times;</span>-->
                <span class="glyphicon glyphicon-remove" style="color:red !important" aria-hidden="true"></span>
                </button>
                </h5>
            </div>
            <div class="modal-body">
                <div class="row">
                <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <img src="{{ asset('frontend/images/mr_sarrath kongahage.jpg') }}" alt="..."> 
                            
                        </div>
                </div>
                <div class="col-sm-6 col-md-8">
                    <p>
                        <span style="font-weight: bold">Senior Attorney Mr. Sarrath  Kongahage</span>
                    </p>
                    
                </div>
                </div>
                <p style="text-align: justify;">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                </p>

                <p style="text-align: justify;">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                </p>
                <p style="text-align: justify;">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                </p>
            </div>
            <div class="modal-footer" style="background-color: #337ab7 !important;border-top: 0px !important">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>
    <!-- end message modal popup -->

    <!-- Modal image gallery section -->
    <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="newsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow: auto;">
                <div class="modal-header" style="background-color: #337ab7 !important;border-bottom: 0px !important;">
                    <h5 class="modal-title" id="newsModalLabel" style="font-weight: bold;color: #fff">Image Gallery
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="glyphicon glyphicon-remove" style="color:red !important" aria-hidden="true"></span>
                        </button>
                    </h5>
                </div>
                    
                <div class="modal-body">  
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" >           
                        <div class="carousel slide article-slide" id="article-photo-carousel">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner cont-slider">
                                <div class="item active">
                                <img alt="" title="" src="{{ asset('frontend/images/img_1.jpg') }}">
                                </div>
                                <div class="item">
                                <img alt="" title="" src="{{ asset('frontend/images/img_2.jpg') }}">
                                </div>
                                <div class="item">
                                <img alt="" title="" src="{{ asset('frontend/images/img_3.jpg') }}">
                                </div>
                                <div class="item">
                                <img alt="" title="" src="{{ asset('frontend/images/img_4.png') }}">
                                </div>
                                <div class="item">
                                <img alt="" title="" src="{{ asset('frontend/images/img_2.jpg') }}">
                                </div>
                                <div class="item">
                                <img alt="" title="" src="{{ asset('frontend/images/img_3.jpg') }}">
                                </div>
                                <div class="item">
                                <img alt="" title="" src="{{ asset('frontend/images/img_4.png') }}">
                                </div>
                            </div>
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li class="active" data-slide-to="0" data-target="#article-photo-carousel">
                                <img alt="" title="" src="{{ asset('frontend/images/img_1.jpg') }}">
                                </li>
                                <li class="" data-slide-to="1" data-target="#article-photo-carousel">
                                <img alt="" title="" src="{{ asset('frontend/images/img_2.jpg') }}">
                                </li>
                                <li class="" data-slide-to="2" data-target="#article-photo-carousel">
                                <img alt="" title="" src="{{ asset('frontend/images/img_3.jpg') }}">
                                </li>
                                <li class="" data-slide-to="3" data-target="#article-photo-carousel">
                                <img alt="" title="" src="{{ asset('frontend/images/img_4.png') }}">
                                </li>
                                <li class="" data-slide-to="1" data-target="#article-photo-carousel">
                                <img alt="" title="" src="{{ asset('frontend/images/img_2.jpg') }}">
                                </li>
                                <li class="" data-slide-to="2" data-target="#article-photo-carousel">
                                <img alt="" title="" src="{{ asset('frontend/images/img_3.jpg') }}">
                                </li>
                                <li class="" data-slide-to="3" data-target="#article-photo-carousel">
                                <img alt="" title="" src="{{ asset('frontend/images/img_4.png') }}">
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="background-color: #337ab7 !important;border-top: 0px !important">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>   
    </div>
    <!-- end image modal popup -->

    <!-- Modal video gallery section -->
    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="newsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow: auto;">
                <div class="modal-header" style="background-color: #337ab7 !important;border-bottom: 0px !important;">
                    <h5 class="modal-title" id="newsModalLabel" style="font-weight: bold;color: #fff">Video Gallery
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="glyphicon glyphicon-remove" style="color:red !important" aria-hidden="true"></span>
                        </button>
                    </h5>
                </div>
                    
                <div class="modal-body">  
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" >           
                        <div class="carousel slide article-slide" id="article-photo-carousel">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner cont-slider">
                                <div class="item active">
                                    <video width="600" height="400" controls>
                                        <source src="{{ asset('frontend/video/mov_bbb.mp4') }}" type="video/mp4">
                                        <source src="{{ asset('frontend/video/mov_bbb.ogg') }}" type="video/ogg">
                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                            </div>
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <!-- <li class="active" data-slide-to="0" data-target="#article-photo-carousel">
                                    <video width="300" height="200" controls>
                                        <source src="video/mov_bbb.mp4" type="video/mp4">
                                        <source src="video/mov_bbb.ogg" type="video/ogg">
                                        Your browser does not support the video tag.
                                    </video>
                                </li> -->
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="background-color: #337ab7 !important;border-top: 0px !important">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>   
    </div>
    <!-- end video modal popup -->

    <!-- Modal comment section -->
    <div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="newsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow: auto;max-height:500px;">
                <div class="modal-header" style="background-color: #337ab7 !important;border-bottom: 0px !important;">
                    <h5 class="modal-title" id="newsModalLabel" style="font-weight: bold;color: #fff">Comment
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <!--<span aria-hidden="true" style="color: red">&times;</span>-->
                        <span class="glyphicon glyphicon-remove" style="color:red !important" aria-hidden="true"></span>
                        </button>
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="{{ asset('frontend/images/commentor_1.jpg') }}" alt="...">                                     
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <p>
                                <span style="font-weight: bold">Date : 2017 August 25</span>
                            </p>
                            <p>
                                <span style="font-weight: bold">Comment By: Prof. Senarathna</span>
                            </p>
                        </div>
                    </div>
                    <p style="text-align: justify;">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    </p>

                    <p style="text-align: justify;">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    </p>
                    <p style="text-align: justify;">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    </p>
                </div>
                <div class="modal-footer" style="background-color: #337ab7 !important;border-top: 0px !important">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end comment modal popup -->

    <!-- Modal event section -->
    <div class="modal fade" id="eventModal" tabindex="-1" role="dialog" aria-labelledby="newsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow: auto;max-height:500px;">
                <div class="modal-header" style="background-color: #337ab7 !important;border-bottom: 0px !important;">
                    <h5 class="modal-title" id="newsModalLabel" style="font-weight: bold;color: #fff">Information Technology
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="glyphicon glyphicon-remove" style="color:red !important" aria-hidden="true"></span>
                        </button>
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="{{ asset('frontend/images/img_sm_4.png') }}" alt="..."> 
                                
                            </div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <p>
                            <span style="font-weight: bold">Programe : Information Technology</span>
                        </p>
                        <p>
                            <span style="font-weight: bold">Date and Time : 14 Aug 2017 - 8.00am-11.00am</span>
                        </p>
                        <p>
                            <span style="font-weight: bold">Location : Lecture Hall 08</span>
                        </p>

                        <p>
                            <span style="font-weight: bold">Insitute : Sri Lanka Foundation</span>
                        </p>
                    </div>
                </div>
                <p style="text-align: justify;">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                </p>

                <p style="text-align: justify;">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                </p>
                <p style="text-align: justify;">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                </p>
            </div>
            <div class="modal-footer" style="background-color: #337ab7 !important;border-top: 0px !important">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
                    
        </div>
    </div>
    <!-- end event modal popup -->


    <!-- end modal section -->
            
</body>
</html>