<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use App\Model\admin\role;
use App\Model\admin\permission;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = role::all();
        return view ('backend.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->can('role.create')) {
            $permissions = permission::all();
            return view ('backend.roles.create', compact('permissions'));
         } 
        
        return redirect(route('admin-slf'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation
        //return $request->all();
        $this->validate($request, [
            'name' => 'required|max:50|unique:roles',
        ]);

        $role = new role;

        $role->name = $request->name;
        $role->admin_id = $request->admin_id;

        $role->save();

        $role->permissions()->sync($request->permission);

        return redirect(route('role.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->can('role.update')) {
            $role = role::where('id', $id)->first();
            $permissions = permission::all();
            return view ('backend.roles.edit', compact('role', 'permissions'));
        } 

        return redirect(route('admin-slf'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validation
        $this->validate($request, [
            'name' => 'required|max:50',
        ]);

        $role = role::find($id);

        $role->name = $request->name;
        $role->admin_id = $request->admin_id;

        $role->save();

        $role->permissions()->sync($request->permission);

        return redirect(route('role.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        role::where('id', $id)->delete();
        return redirect()->back();
    }
}
